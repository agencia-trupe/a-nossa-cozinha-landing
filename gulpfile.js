var gulp        = require('gulp'),
    stylus      = require('gulp-stylus'),
    koutoSwiss  = require('kouto-swiss');

gulp.task('css', function() {
    return gulp.src(['./assets/css/main.styl'])
        .pipe(stylus({
            use: [koutoSwiss()],
            compress: true
        }))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('default', function() {
    gulp.watch('./assets/css/main.styl', ['css']);
});
