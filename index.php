<?php

require "vendor/autoload.php";

use \PHPMailer;
use \Respect\Validation\Validator as v;

$nome     = isset($_POST['nome']) ? $_POST['nome'] : null;
$email    = isset($_POST['email']) ? $_POST['email'] : null;
$telefone = isset($_POST['telefone']) ? $_POST['telefone'] : null;
$mensagem = isset($_POST['mensagem']) ? $_POST['mensagem'] : null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $validaNome     = v::string()->notEmpty()->validate($nome);
    $validaEmail    = v::email()->validate($email);
    $validaTelefone = v::string()->validate($telefone);
    $validaMensagem = v::string()->notEmpty()->validate($mensagem);

    if (!$validaNome || !$validaEmail || !$validaTelefone || !$validaMensagem) {

        $response = 'Preencha os campos corretamente.';

    } else {

        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->Host = 'smtp.anossacozinha.com.br';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@anossacozinha.com.br';
        $mail->Password = '8ZK8c58j';
        $mail->Port = 25;

        $mail->setFrom('noreply@anossacozinha.com.br', 'A Nossa Cozinha');
        $mail->addAddress('contato@anossacozinha.com.br', 'A Nossa Cozinha');
        $mail->addReplyTo($email, $nome);

        $mail->isHTML(true);

        $mail->Subject = '[CONTATO] A Nossa Cozinha';
        $mail->Body    = '<strong>Nome:</strong> '. $nome .'<br><strong>Telefone:</strong> '. $telefone .'<br><strong>E-mail:</strong> '. $email .'<br><strong>Mensagem:</strong> '. $mensagem .'<br>';

        if(!$mail->send()) {
            $response = 'Ocorreu um erro. Tente novamente.';
            // $response = $mail->ErrorInfo;
        } else {
            $response = 'Mensagem enviada com sucesso!';
            $nome     = null;
            $email    = null;
            $telefone = null;
            $mensagem = null;
        }

    }
}

?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="<?=date('Y')?> Trupe Agência Criativa">
    <meta name="description" content="Espaço cultural Gourmet. Rotisserie online.">
    <meta name="keywords" content="culinária, cursos de culinária, espaço gourmet, rotisserie, locação buffet, utensílios de cozinha, pratos prontos, encomenda de comida, menus, locação de cozinha industrial, locação de equipamentos, exposição e venda de produtos gourmet, personal chef, treinamento de empregadas domésticas, dinâmicas de grupo, salas de reunião">

    <title>A Nossa Cozinha</title>

    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <div class="center">
        <div class="chamada">
            <h1>A Nossa Cozinha</h1>
            <p class="frase">Estamos reformulando nosso site. Enquanto isso visite nossa LOJA VIRTUAL</p>
            <a href="http://www.anossacozinha.com.br/loja">Comidinhas Online</a>
            <p class="consulte">Consulte-nos também sobre orçamentos para eventos!</p>
        </div>

        <div class="informacoes">
            <p>Fale conosco sobre outros assuntos:</p>
            <p class="telefone">11 5533·4181</p>
            <a href="mailto:contato@anossacozinha.com.br" class="email">contato@anossacozinha.com.br</a>
            <a href="https://www.facebook.com/anossacozinha" class="facebook">
                <span class="texto">Nos acompanhe<br>nas redes sociais!</span>
                <span class="logo"></span>
            </a>
            <p>Nosso endereço:</p>
            <p class="endereco">Rua Assis Brasil, 99, Brooklin - SP, São Paulo</p>
        </div>

        <form action="" method="POST">
            <?php if (isset($response)): ?>
            <div class="mensagem"><?=$response?></div>
            <?php endif; ?>
            <input type="text" name="nome" placeholder="Nome" value="<?=$nome?>" required>
            <input type="text" name="telefone" placeholder="Telefone" value="<?=$telefone?>">
            <input type="email" name="email" placeholder="E-mail" value="<?=$email?>" required>
            <textarea name="mensagem" placeholder="Mensagem" required><?=$mensagem?></textarea>
            <input type="submit" value="Enviar">
        </form>
    </div>

    <div class="googlemaps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.5353079160564!2d-46.6824442!3d-23.620993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50bd744da321%3A0xed2e98b786a02980!2sR.+Assis+Brasil%2C+99+-+Campo+Belo%2C+S%C3%A3o+Paulo+-+SP%2C+04601-010!5e0!3m2!1spt-BR!2sbr!4v1444145487477" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-27300595-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>